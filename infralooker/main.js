//define public or internal search engines here, following the example.
const search_engines = [
	"https://kubernetes.io/search/?q=", 
	"https://cloud.google.com/s/results?hl=en&q="
]

//Search whenever button is clicked.
document.getElementById('searchButton').addEventListener('click', function(){
	var string = document.getElementById("userInput").value
	search_engines.forEach(function(engine){
		chrome.tabs.create({url: engine + string});
	});
});

// About button action
document.getElementById('aboutButton').addEventListener('click', function(){
	alert('Created by Andreu Pasalamar Carbó');
});